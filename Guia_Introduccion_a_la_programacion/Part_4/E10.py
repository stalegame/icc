def main():
    base = int(input("Ingrese una base: "))
    exp = int(input("Ingrese el exponente: "))
    potencia = 1
    for i in range(0, exp):
        potencia = base * potencia
        print(f"{potencia}") 


if __name__ == '__main__':
    main()