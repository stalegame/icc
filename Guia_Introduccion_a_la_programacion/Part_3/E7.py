def check_day(num):

    if num == 1:
        print("Monday")

    elif num == 2:
        print("Tuesday")

    elif num == 3:
        print("Wednesday")

    elif num == 4:
        print("Thursday")

    elif num == 5:
        print("Friday")

    elif num == 6:
        print("Saturday")

    elif num == 7:
        print("Sunday")

    else:
        print("Invalido")


def main():
    print("Bienvenido a calculadora de dias de la semana")
    num = int(input("Ingrese un numero del 1 al 7: "))
    check_day(num)


if __name__ == '__main__':
    main()
