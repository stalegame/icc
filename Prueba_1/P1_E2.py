import random
random.seed

def rolldice():
    return random.randint(1, 6)
     

def add(d1, d2):
    return d1 + d2
       

def main():
    print("Bienvenido a la apuesta")
    bet = int(input("Ingrese su apuesta: "))
    
    print("Turno 1")
    d1 = rolldice()
    d2 = rolldice()
    suma = add(d1, d2)     
    print(f"Usted saco {d1} y {d2}, esto suma {suma}")
    if suma == 7:
        bet = bet * 2
        print(f"Usted saco 7 gana el doble de lo apostado {bet}")
    
    elif suma == 11:
        bet = bet * 1.5
        print(f"Usted saco 11 gana la mitad de lo apostado {bet}")

    else:
        ("\n")
        print("Turno 2")
        d1_t2 = rolldice()
        d2_t2 = rolldice()
        suma_2 = add(d1_t2, d2_t2)
        print(f"Usted saco {d1_t2} y {d2_t2}, esto suma {suma_2}")
        if suma_2 == 7:
            bet = 0
            print(f"Seven out, perdio todo {bet}")
        
        elif suma_2 == 11:
            bet = 0
            print(f"perdio todo {bet}")
        
        elif suma == suma_2:
            bet = bet * 1.5
            print(f"gana la mitad de lo apostado {bet}")

        else:
            bet = bet * 0.5
            print(f"Perdio la mitad de lo apostado {bet}")
               
                 

if __name__ == "__main__":
    main()
