def check_note(note):

    if note >= 1 and note <= 3.9:
        print("Nota insuficiente")

    elif note >= 4 and note <= 4.9:
        print("Nota suficiente")

    elif note >= 5 and note <= 5.9:
        print("Nota buena")

    elif note >= 6 and note <= 6.4:
        print("Nota notable")

    elif note >= 6.5 and note <= 7:
        print("Nota Sobresaliente")

    else:
        print("Lo ingresado no es valido")

def main():
    print("Bienvenido al clasificador de notas")
    note = float(input("Ingrese su nota: "))
    check_note(note)


if __name__ == '__main__':
    main()
