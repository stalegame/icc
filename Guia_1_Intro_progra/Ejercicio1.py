def calculate_tax(money, age, gender):

    if gender == "masculino" and age < 65:

        if money <= 100000:
            print(f"No se aplica impuesto, su monto final es de {money}")

        elif money >= 100001 and money <= 1000000:
            money = money - (money * 0.10)
            print(f"Se le aplica un impuesto del 10%, su monto final es de {money}")

        elif money >= 1000001 and money <= 6000000:
            money = money - (money * 0.20)
            print(f"Se le aplica un impuesto del 20%, su monto final es de {money}")

        elif money > 6000000:
            money = money - (money * 0.30)
            print(f"Se le aplica un impuesto del 30%, su monto final es de {money}")

    else:

        if money < 2000000:
            print(f"Usted no paga impuesto por su edad {age} años y gana menos de 2M")
            exit()

        else:
            money = money - (money * 0.20)
            print(f"Usted paga impuesto ya que gana más de 2M, su monto final es de {money}")
            exit()

    if gender == "femenino" and age < 60:

        if money <= 100000:
            print(f"No se aplica impuesto, su monto final es de {money}")

        elif money >= 100001 and money <= 1000000:
            money = money - (money * 0.10)
            print(f"Se le aplica un impuesto del 10%, su monto final es de {money}")

        elif money >= 1000001 and money <= 6000000:
            money = money - (money * 0.20)
            print(f"Se le aplica un impuesto del 20%, su monto final es de {money}")

        elif money > 6000000:
            money = money - (money * 0.30)
            print(f"Se le aplica un impuesto del 30%, su monto final es de {money}")

    else:

        if money < 2000000:
            print(f"Usted no paga impuesto por su edad {age} años y gana menos de 2M")
            exit()

        else:
            money = money - (money * 0.20)
            print(f"Usted paga impuesto ya que gana más de 2M, su monto final es de {money}")
            exit()


def main():
    print("Bienvenido a la calculadora de impuesto")
    age = int(input("Ingrese su edad: "))
    gender = input("Ingrese su genero(masculo/femenino): ")
    money = int(input("Ingrese la cantidad de dinero (clp): "))
    calculate_tax(money, age, gender)


if __name__ == '__main__':
    main()
