def comprobar(x, y, z):
    #equilatero x = y = z
    if x == y == z:
        print("El triangulo es equilatero")

    #isoceles x = y !=  z
    elif x == y != z:
        print("El triangulo es isoceles")

    #escaleno x != y != z
    else:
        print("El triangulo es escaleno")


def main():
    print("Bienvenido a la aplicacion para comprobar triangulos")
    side_1 = float(input("Ingrese el largo del primer lado: "))
    side_2 = float(input("Ingrese el largo del segundo lado: "))
    side_3 = float(input("Ingrese el lado del tercer lado: "))
    comprobar(side_1, side_2, side_3)


if __name__ == '__main__':
    main()
