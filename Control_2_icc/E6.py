def Part_fraccion(numero):
    parte_entera = int(numero)
    parte_fraccion = numero - parte_entera

    p_f_b = ''
    while parte_fraccion != 0:
        parte_fraccion *= 2
        bit = int(parte_fraccion)
        p_f_b += str(bit)
        parte_fraccion -= bit
        
        if len(p_f_b) > 12:
            break
    
    return p_f_b 


def decimal_a_binario(numero):
    numero = int(numero)
    if (numero) == 0:
        return '00000000'

    binario = ''
    while (numero) > 0:
        resto = numero % 2
        binario = str(resto) + binario
        numero = numero //2

    if len(binario) < 8:
        binario = '0' * (8 - len(binario)) + binario

    return binario


def main():
    numero = float(input("Ingrese un numero entero fraccionario: "))
    part_entera = decimal_a_binario(numero)
    Par_fraccion = Part_fraccion(numero)
    print(f"El numero decimal {numero} en binario es: {part_entera + '.' + Par_fraccion} ")


if __name__ == '__main__':
    main()