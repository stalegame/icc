def conversor(tem, uni, aux=None):
    if uni == "celsius":
        aux = (tem * 9/5) + 32
        print(f"Su temperatura en fahrenheit es de {aux:.2f}")

    elif uni == "fahrenheit":
        aux = (tem - 32) * 5/9
        print(f"Su temperatura en celsius es de {aux:.2f}")

    else:
        print("unidad invalida")


def main():
    print("Bienvenido a la calculadora de temperatura")
    tem = float(input("Ingrese la temperatura: "))
    uni = input("Ingrese la unidad de medida (fahrenheit o celsius): ")
    conversor(tem, uni)


if __name__ == '__main__':
    main()
