#Thomas aranguiz

def budget(x, y, z = None):
    if y < 5:
        if x < 5:
            z = 500
            return z
        
        elif x >= 5 and x < 10:
            z = 1000
            return z
        
        else:
            z = 3000
            return z
    if y >= 5:

        if x < 5:
            z = 500 + (500 * 0.2)
            return z
        
        elif x >= 5 and x < 10:
            z = 1000 + (1000 * 0.2)
            return z
        
        else:
            z = 3000 + (3000 * 0.2)
            return z


def discount(outlet, total):
    if outlet == "si":
        total = total - (total * 0.10)
        return total
    
    else:
        return total 


def ticket(accident_distance, job, outlet, total_d):
    if outlet == "si":
        return print(f"Por la distancia {accident_distance}km, el trabajo {job}h, y su auto si es de consecionario tiene que pagar {total_d}")
    else:
        return print(f"Por la distancia {accident_distance}km, el trabajo {job}h, y su auto no de consecionario tiene que pagar {total_d}")


def main():
    print("Bienvenido al taller mecanico")
    accident_distance = int(input("Ingrese la distancia de donde se encuentra del taller(km): "))
    job = int(input("Ingrese la cantidad de trabajo que necesita(horas): "))
    outlet = input("Ingrese si su vehiculo es de consecionario: ")
    print("\n")

    total = budget(accident_distance, job)
    total_d = discount(outlet, total)
    ticket(accident_distance, job, outlet, total_d)
    

if __name__ == "__main__":
    main()
