def aplicar_descuento(price, cat, aux = None):
    if cat == "estudiante":
        aux = price - (price * 0.1)
        print(f"El valor original era de {price} y con el descuento quedo en {aux}")

    if cat == "adulto":
        aux = price - (price * 0.2)
        print(f"El valor original era de {price} y con el descuento quedo en {aux}")

    if cat == "mayor":
        aux = price - (price * 0.3)
        print(f"El valor original era de {price} y con el descuento quedo en {aux}")


def main():
    print("Bienvenido a la calculadora de descuento")
    price = float(input("Ingrese el precio de un producto: "))
    cat = input("Ingrese su categoria (estudiante, adulto o mayor): ")
    aplicar_descuento(price, cat)


if __name__ == '__main__':
    main()
