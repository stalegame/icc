import random
random.seed()

#Funcion para retirar el valor de la carta
def choose():
    card = random.randint(1,14)
    return card

#Funcion para validar la pinta de la carta
def validate_pint(card):

    if card == 1:
        return ("A")

    elif card == 11:
        return ("J")

    elif card == 12:
        return ("Q")

    elif card == 13:
        return ("K")

    elif card == 14:
        return ("Jajas")

    else:
        return card

#Funcion para ejecutar el juego
def main():
    print("Bienvenido al juego de cartas")

    game = 0
    adder_1 = 0
    adder_2 = 0
    #Ciclo de control para el juego
    while game != 3:

        #Bloque donde los jugadores retiran su carta en cada turno
        print(f"turno {game+1}")
        player_1 = choose()
        adder_1 = adder_1 + player_1
        print(f"Jugador 1 saca {validate_pint(player_1)}")

        player_2 = choose()
        adder_2 = adder_2 + player_2
        print(f"Jugador 2 saca {validate_pint(player_2)}")

        #Validacion de victoria x ronda
        if player_1 == player_2:
            print("Empate")

        elif player_1 > player_2:
            print("Gana la ronda jugar 1")

        else:
            print("Gana la ronda jugador 2")
        print("\n")
        game = game+1

    #validacion ganador juego en general
    if adder_1 > adder_2:
        print(f"Gana el jugador 1 con {adder_1} puntos")

    elif adder_1 == adder_2:
        print(f"Empatan con {adder_1} puntos")

    else:
        print(f"Gana el jugador 2 con {adder_2} puntos")


if __name__ == '__main__':
    main()
