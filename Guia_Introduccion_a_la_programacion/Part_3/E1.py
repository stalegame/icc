def Calculator_IMC(W = None ,h= None):
    IMC = W/(h**2)

    if IMC >= 0 and IMC <= 18.5:
        print(f"Usted se encuentra en bajo peso con un IMC de {IMC:.2f}")

    elif 18.6 >= IMC or IMC <= 24.9:
        print(f"Usted se encuentra en un peso adecuado con un IMC de {IMC:.2f}")

    elif 25.0 >= IMC or IMC <= 29.9:
        print(f"Usted se encuentra en sobrepeso con un IMC de {IMC:.2f}")

    elif 30.0 >= IMC or IMC <= 34.9:
        print(f"Usted se encuentra en obesidad grado 1 con un IMC de {IMC:.2f}")

    elif 35.0 >= IMC or IMC <= 39.9:
        print(f"Usted se encuentra en obesidad grado 2 con un IMC de {IMC:.2f}")

    else:
        print(f"Usted se encuentra en obesidad grado 3 con un IMC de {IMC:.2f}")

    return


def main():
    print("Bienvenido a la calculadora de IMC")
    p = float(input("Ingrese su peso en kilogramos: "))
    h = float(input("Ingrese su altura en metros: "))
    Calculator_IMC(p, h)

if __name__ == '__main__':
    main()
