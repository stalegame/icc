def check_data(x, y, z):

    if x**2 + y**2 == z**2:

        if x == y == z:
            print("triangulo equilatero")

        elif x == y != z:
            print("triangulo isoseles")

        else:
            print("triangulo escaleno")

    else:
        print("medidas invalidas para hacer un triangulo")


def main():
    print("Bienvenido a la calculadora de triangulos")
    large_1 = float(input("Ingrese longitud 1: "))
    large_2 = float(input("Ingrese longitud 2: "))
    large_3 = float(input("Ingrese longitud 3: "))
    check_data(large_1, large_2, large_3)


if __name__ == '__main__':
    main()
