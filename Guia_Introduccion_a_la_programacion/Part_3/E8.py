def check_age(age):

    if age < 18:
        print("Usted es menor de edad")

    elif age >= 18:
        print("Usted es mayor de edad")


def main():
    print("Bienvenido al verificador de edad: ")
    age = int(input("Ingrese su edad: "))
    check_age(age)


if __name__ == '__main__':
    main()
