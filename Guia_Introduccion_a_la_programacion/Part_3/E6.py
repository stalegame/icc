def comprobar(num):
    for i in range(2, num):
        if num % i == 0:
            print(f"No es primo {i} es divisor")
        return False
    print("Es primo")
    return True


def recursividad(num, n=2):
    if n >= num:
        print("Es primo")
        return True

    elif num % n != 0:
        return recursividad(num, n + 1)

    else:
        print(f"No es primo {n} es divisor")
        return False


def main():
    print("Bienvenido a la aplicacion para comprobar numeros primos")
    num = int(input("Ingrese un numero para comprobar si es primo: "))
    comprobar(num)
    recursividad(num)

if __name__ == '__main__':
    main()
