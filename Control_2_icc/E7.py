def binario_a_decimal(binario):
    if len(binario) < 8:
        binario = "0" * (8 - len(binario)) + binario

    suma = 0
    for i in range(len(binario)):
        bit = int(binario[i])
        suma += bit * (2 ** (len(binario) - 1 - i))     
                   
    return suma        


def main():
    binario = input("Ingrese un numero en binario: ")
    decimal = binario_a_decimal(binario)
    print(decimal)

if __name__ == '__main__':
    main()