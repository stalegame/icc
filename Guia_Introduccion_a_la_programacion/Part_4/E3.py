def main():
    num = int(input("Ingrese un numero: "))
    multiplicacion = 0
    for i in range(1, 11):
        multiplicacion = i * num
        print(f"{i} * {num} = {multiplicacion}") 


if __name__ == '__main__':
    main()