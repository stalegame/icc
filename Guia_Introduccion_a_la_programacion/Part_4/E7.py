def main():
    num = int(input("Ingrese numeros y se contaran que tan largo es el numero: "))
    num_str = str(num)
    suma = 0
    for i in num_str:
        suma += int(i)
    print(f"El numero ingresado es {num} y la suma de sus digitos es {suma}")     


if __name__ == '__main__':
    main()