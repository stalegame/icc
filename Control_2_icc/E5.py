def complemento_a_2(ca1):
    ca2 = ''
    aux = 1
    for bit in ca1[::-1]:
        if bit == '0' and aux == 1:
            ca2 = '1' + ca2   
            aux = 0

        elif bit == '1' and aux == 1:
            ca2 = '0' + ca2

        else:
            ca2 = bit + ca2

    return ca2            

def complemento_a_1(binario):
    ca1 = ''
    for bit in binario:
        if bit == '0':
            ca1 += '1'
        else:
            ca1 += '0'    
    return ca1


def decimal_a_binario(numero):
    if numero == 0:
        return '00000000'

    binario = ''
    while numero > 0:
        resto = numero % 2
        binario = str(resto) + binario
        numero = numero //2

    if len(binario) < 8:
        binario = '0' * (8 - len(binario)) + binario

    return binario 


def main():
    numero = int(input("Ingrese un numero entero negativo: "))
    valor_absoluto = abs(numero)
    binario = decimal_a_binario(valor_absoluto)
    print(f"El numero con valor absoluto {valor_absoluto} en binario es {binario}")
    ca1 = complemento_a_1(binario)
    print(f"El numero en complemento a uno es {ca1}")
    ca2 = complemento_a_2(ca1)
    print(f"El complemento a dos del numero {numero} es {ca2}")


if __name__ == '__main__':
    main()