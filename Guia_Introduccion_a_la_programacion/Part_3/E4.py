def comprobar(year):
    # Si el no es divisible por 4 no es bisiesto
    if year % 4 != 0:
        print("No es año bisiesto")

    #Si es divisible por 4 y no por 100 es bisiesto
    elif year % 4 == 0 and year % 100 != 0:
        print("Es año bisiesto")

    #si es divisible por 4 y por 100 pero no por 400 no es bisiesto
    elif year % 4 == 0 and year % 100 == 0 and year %   400 != 0:
        print("No es año bisiesto")

    #si es divisible por 4, 100  y 400 es bisiesto
    elif year % 4 == 0 and year % 100 == 0 and year %   400 == 0:
        print("Es año bisiesto")


def segunda_opcion(year):
    #si es divisible por 4 no por 100 y si por 400 es bisiesto
    if year % 4 == 0 and (year % 100 != 0 or year % 400 == 0):
        print("Es año bisiesto")
    else:
        print("No es bisiesto")


def main():
    print("Bienvenido a la aplicacion para comprobar años bisiesto")
    year = int(input("Ingrese un año: "))
    comprobar(year)
    segunda_opcion(year)


if __name__ == '__main__':
    main()
