def check_num(num):

    if num < 0:
        print("Es negativo")

    elif num == 0:
        print("Es cero")

    else:
        print("Es positivo")


def main():
    print("Bienvenido al verificador de signos: ")
    num = float(input("Ingrese un numero: "))
    check_num(num)


if __name__ == '__main__':
    main()
