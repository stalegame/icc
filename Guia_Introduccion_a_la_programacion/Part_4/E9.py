def fib(n):
    if n < 2:
        return n
    else:
        return fib(n-1) + fib(n-2)


def main():
    n = int(input("Ingrese un numero para la secuencia de fibonashi: "))
    for i in range(n):
        print(fib(i))

if __name__ == '__main__':
    main()