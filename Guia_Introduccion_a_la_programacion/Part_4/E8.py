def main():
    num = int(input("Ingrese numeros y se contaran que tan largo es el numero: "))
    num_str = str(num)
    contador_p = 0
    contador_i = 0
    for i in num_str:
        if int(i) % 2 == 0:
            contador_p += 1
        else:
            contador_i += 1

    print(f"El numero ingresado es {num}, tiene {contador_p} numeros pares y {contador_i} numeros impares")         


if __name__ == '__main__':
    main()